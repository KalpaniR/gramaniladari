package com.gramaniladari.app.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "timber")
public class TimberTransport extends CommonService implements Serializable{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtimber")
    private Long id;

    @Column(name = "Buyer_name")
    private String buyerName;
    @Column(name = "Buyer_address")
    private String buyerAddress;
    @Column(name = "warehouse_name")
    private String warehouseName;
    @Column(name = "registered_no")
    private String registeredNo;
    @Column(name = "land_name")
    private String landName;
    @Column (name = "File_No")
    private String fileNo;
    @Column(name = "registered_office")
    private String registeredOffice;
    @Column(name = "to_north")
    private String toNorth;
    @Column(name = "to_east")
    private String toEast;
    @Column(name = "to_west")
    private String toWest;
    @Column(name = "to_south")
    private String toSouth;
    @Column(name = "land_owner")
    private String landOwner;
    @Column(name = "land_owner_address")
    private String landOwnerAdress;
    @Column(name = "timber_type")
    private String timberType;
    @Column(name = "date_of_transport")
    private String dateOfTransport;
    @Column(name = "services_service_id")
    private String serviceId;
    
    
    @OneToOne
    @MapsId
    private GNService gnService;


	public TimberTransport(String buyerName, String buyerAddress) {
		super();
		this.buyerName = buyerName;
		this.buyerAddress = buyerAddress;
	}


	public GNService getGnService() {
		return gnService;
	}


	public void setGnService(GNService gnService) {
		this.gnService = gnService;
	}
    
	
    
    
}
