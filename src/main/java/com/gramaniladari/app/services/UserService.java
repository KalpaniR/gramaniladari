package com.gramaniladari.app.services;

import java.util.ArrayList;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gramaniladari.app.models.Role;
import com.gramaniladari.app.models.User;
import com.gramaniladari.app.repositories.RoleRepository;
import com.gramaniladari.app.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
   // @Autowired
//    private BCryptPasswordEncoder bCryptPasswordEncoder;

    
	public void save(User user) {
//		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	//	user.setRoles(new HashSet<>(roleRepository.findAll()));
		
		//ArrayList<Role> roles = new ArrayList<>();
		//Role r1 = new Role();
		//r1.setName("ADMIN");
		//r1.setId(2837462L);
		//roles.add(r1);
		
		//user.setRoles(new HashSet<>(roles));
		userRepository.save(user);

	}

	public User findByNic(String nic) {
	    return userRepository.findByNic(nic);
	}
	
}
