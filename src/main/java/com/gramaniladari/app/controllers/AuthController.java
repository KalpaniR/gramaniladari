package com.gramaniladari.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.gramaniladari.app.models.User;
import com.gramaniladari.app.services.UserService;

@Controller
public class AuthController {

	@Autowired
	private UserService userService;
		
	@GetMapping("/")
	public String index(Model model) {
        return "index";
    }
	
	@GetMapping("/login")
	public String login(Model model) {
        return "pages/examples/login";
    }
	
	@GetMapping("/register")
	public String register(Model model) {
		model.addAttribute("userForm",new User());
        return "pages/forms/register";
    }
	
	@PostMapping("/register")
    public String register(@ModelAttribute("userForm")User userForm, BindingResult bindingResult) {
        //userValidator.validate(userForm, bindingResult);
//		@ModelAttribute("userForm")
//        if (bindingResult.hasErrors()) {
//            return "pages/examples/register";
//        }
		
		
        userService.save(userForm);

        //securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());
        return "index";
    }

	
	
	@GetMapping("/business")
	public String business(Model model) {
        return "pages/examples/business";
    }
	
	@GetMapping("/timber")
	public String timber(Model model) {
        return "pages/forms/timber";
    }
	
	@GetMapping("/treecutting")
	public String treecuting(Model model) {
        return "pages/examples/treecuting";
    }
	
	@GetMapping("/businessfirm")
	public String businessfirm(Model model) {
        return "pages/examples/businessfirm";
    }
	
    @GetMapping("/gunlisence")
	public String gunlisence(Model model) {
        return "pages/examples/gunlisence";
    }
	
    
}
